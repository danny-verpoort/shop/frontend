import axios, {AxiosError} from "axios";
import NextAuth from "next-auth";
import {JWT} from "next-auth/jwt";
import KeycloakProvider from "next-auth/providers/keycloak";

const keycloak = KeycloakProvider({
  clientId: process.env.KEYCLOAK_ID || "",
  clientSecret: process.env.KEYCLOAK_SECRET || "",
  issuer: process.env.KEYCLOAK_ISSUER,
});

async function doFinalSignoutHandshake(jwt: JWT) {
  const {provider, id_token} = jwt;
  try {
    // Add the id_token_hint to the query string
    const params = new URLSearchParams();
    params.append('id_token_hint', id_token || "");
    const {
      status,
      statusText
    } = await axios.get(`${keycloak.options?.issuer}/protocol/openid-connect/logout?${params.toString()}`);

    // The response body should contain a confirmation that the user has been logged out
  } catch (e: any) {
    console.error("Unable to perform post-logout handshake", (e as AxiosError)?.code || e)
  }
}

export default NextAuth({
  debug: true,
  secret: process.env.NEXTAUTH_SECRET,
  providers: [
    keycloak
  ],
  callbacks: {
    jwt: async ({token, user, account, profile, isNewUser}) => {
      if (account) {
        token.exp = account.expires_at;
        token.id_token = account.id_token;
      }

      return token;
    }
  },
  events: {
    signOut: ({session, token}) => doFinalSignoutHandshake(token)
  }
});