import Application from "../containers/application/application";
import {CubeIcon, ExclamationTriangleIcon, PersonIcon, PlusIcon, RocketIcon} from "@radix-ui/react-icons";
import {useSession} from "next-auth/react";

export default function Index() {
  const {data: session} = useSession()
  return (
      <Application>
        <main className="space-y-8">
          <header className="text-zinc-600">
            <h1 className="text-lg font-semibold">Hi, {session?.user?.name}!</h1>
            <h3>Have a look at today&apos;s stock status</h3>
          </header>
          <section
              className="px-6 py-6 grid grid-cols-1 gap-12 lg:grid-cols-2 xl:grid-cols-4 bg-gradient-to-tl from-rose-500 to-rose-400">
            <article className="bg-white shadow-lg p-4 overflow-x-hidden">
              <div className="flex justify-between">
                <header>
                  <h5 className="text-blueGray-400 uppercase font-bold text-xs">Users</h5>
                  <span className="font-semibold text-xl text-blueGray-700">1,500,000</span>
                  <p className="text-sm mt-5">
                    <span className="text-rose-500 mr-2"><i className="fas fa-arrow-up"></i> -1% </span>
                    <span className="whitespace-nowrap"> Since last month </span>
                  </p>
                </header>
                <aside>
                  <div
                      className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-red-500">
                    <PersonIcon/>
                  </div>
                </aside>
              </div>
            </article>
            <article className="bg-white shadow-lg p-4 overflow-x-hidden">
              <div className="flex justify-between">
                <header>
                  <h5 className="text-blueGray-400 uppercase font-bold text-xs"> Sales</h5>
                  <span className="font-semibold text-xl text-blueGray-700">53</span>
                  <p className="text-sm mt-5">
                    <span className="text-emerald-500 mr-2"><i className="fas fa-arrow-up"></i> 5,8% </span>
                    <span className="whitespace-nowrap"> Since last month </span>
                  </p>
                </header>
                <aside>
                  <div
                      className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-cyan-500">
                    <RocketIcon/>
                  </div>
                </aside>
              </div>
            </article>
            <article className="bg-white shadow-lg p-4 overflow-x-hidden">
              <div className="flex justify-between">
                <header>
                  <h5 className="text-blueGray-400 uppercase font-bold text-xs">Revenue</h5>
                  <span className="font-semibold text-xl text-blueGray-700">&euro; 220,10</span>
                  <p className="text-sm mt-5">
                    <span className="text-emerald-500 mr-2"><i className="fas fa-arrow-up"></i> 0,2% </span>
                    <span className="whitespace-nowrap"> Since last month </span>
                  </p>
                </header>
                <aside>
                  <div
                      className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-red-500">
                    <PersonIcon/>
                  </div>
                </aside>
              </div>
            </article>
            <article className="bg-white shadow-lg p-4 overflow-x-hidden">
              <div className="flex justify-between">
                <header className="space-y-2">
                  <div>
                    <h5 className="text-blueGray-400 uppercase font-bold text-xs">Categories</h5>
                    <span className="font-semibold text-xl text-blueGray-700">59</span>
                  </div>
                  <div>
                    <h5 className="text-blueGray-400 uppercase font-bold text-xs">Products</h5>
                    <span className="font-semibold text-xl text-blueGray-700">1,983</span>
                  </div>
                </header>
                <aside>
                  <div
                      className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-red-500">
                    <CubeIcon/>
                  </div>
                </aside>
              </div>
            </article>
          </section>
          <section className="grid grid-cols-1 md:grid-cols-2 gap-12">
            <article>
              <h1 className="font-semibold"><RocketIcon className="inline"/> Popular products</h1>
              <main className="pt-6">
                <div className="relative overflow-x-auto shadow-md">
                  <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                      <th scope="col" className="px-6 py-3">
                        Product name
                      </th>
                      <th scope="col" className="px-6 py-3">
                        Category
                      </th>
                      <th scope="col" className="px-6 py-3">
                        Price
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Apple MacBook Pro 17&quot;
                      </th>
                      <td className="px-6 py-4">
                        Laptop
                      </td>
                      <td className="px-6 py-4">
                        $2999
                      </td>
                    </tr>
                    <tr className="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Microsoft Surface Pro
                      </th>
                      <td className="px-6 py-4">
                        Laptop PC
                      </td>
                      <td className="px-6 py-4">
                        $1999
                      </td>
                    </tr>
                    <tr className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Magic Mouse 2
                      </th>
                      <td className="px-6 py-4">
                        Black
                      </td>
                      <td className="px-6 py-4">
                        Accessories
                      </td>
                    </tr>
                    <tr className="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Google Pixel Phone
                      </th>
                      <td className="px-6 py-4">
                        Phone
                      </td>
                      <td className="px-6 py-4">
                        $799
                      </td>
                    </tr>
                    <tr>
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Apple Watch 5
                      </th>
                      <td className="px-6 py-4">
                        Wearables
                      </td>
                      <td className="px-6 py-4">
                        $999
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </main>
            </article>
            <article>
              <h1 className="leading-6 font-semibold"><ExclamationTriangleIcon className="inline mr-1"/> Low on stock
              </h1>
              <main className="pt-6">
                <div className="relative overflow-x-auto shadow-md">
                  <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                      <th scope="col" className="px-6 py-3">
                        Product name
                      </th>
                      <th scope="col" className="px-6 py-3">
                        Category
                      </th>
                      <th scope="col" className="px-6 py-3">
                        Price
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Apple MacBook Pro 17&quot;
                      </th>
                      <td className="px-6 py-4">
                        Laptop
                      </td>
                      <td className="px-6 py-4">
                        $2999
                      </td>
                    </tr>
                    <tr className="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Microsoft Surface Pro
                      </th>
                      <td className="px-6 py-4">
                        Laptop PC
                      </td>
                      <td className="px-6 py-4">
                        $1999
                      </td>
                    </tr>
                    <tr className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Magic Mouse 2
                      </th>
                      <td className="px-6 py-4">
                        Black
                      </td>
                      <td className="px-6 py-4">
                        Accessories
                      </td>
                    </tr>
                    <tr className="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Google Pixel Phone
                      </th>
                      <td className="px-6 py-4">
                        Phone
                      </td>
                      <td className="px-6 py-4">
                        $799
                      </td>
                    </tr>
                    <tr>
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        Apple Watch 5
                      </th>
                      <td className="px-6 py-4">
                        Wearables
                      </td>
                      <td className="px-6 py-4">
                        $999
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </main>
            </article>
          </section>
          <section>
            <article>
              <h1 className="leading-6 font-semibold"><PlusIcon className="inline mr-1"/>New users</h1>
              <main className="pt-6">
                <div className="relative overflow-x-auto shadow-md">
                  <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                      <th scope="col" className="px-6 py-3">
                        Username
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        John Doe
                      </th>
                    </tr>
                    <tr className="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        John Doe
                      </th>
                    </tr>
                    <tr className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        John Doe
                      </th>
                    </tr>
                    <tr className="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        John Doe
                      </th>
                    </tr>
                    <tr>
                      <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        John Doe
                      </th>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </main>
            </article>
          </section>
        </main>
      </Application>
  )
}