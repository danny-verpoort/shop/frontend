FROM node:18.16.0-alpine3.18

WORKDIR /var/app

COPY . /var/app

RUN yarn install
RUN yarn build