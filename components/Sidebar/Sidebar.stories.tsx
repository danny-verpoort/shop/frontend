import type { Meta, StoryObj } from "@storybook/react"
import { Sidebar } from "./Sidebar"

const meta: Meta<typeof Sidebar> = {
    title: "Sidebar",
    component: Sidebar,
    args: {
        intent: "primary",
        expanded: false
    },
    argTypes: {
        intent: {
            options: ["primary"],
            control: { type: "select" },
        },
        expanded: {type: "boolean"}
    },
}

type Story = StoryObj<typeof Sidebar>

export const Default: Story = {
    render: (args) => <Sidebar {...args} >Test</Sidebar>,
    parameters: {
        layout: "fullscreen"
    }
}

export default meta