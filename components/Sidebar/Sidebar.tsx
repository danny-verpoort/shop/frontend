import {cva, type VariantProps} from "class-variance-authority"

import {twMerge} from "tailwind-merge"
import {Button} from "../Button/Button";
import {DoubleArrowLeftIcon} from "@radix-ui/react-icons";

const sidebar = cva(
    [
      "bg-teal-500",
      "h-[calc(100dvh)]",
      "transition-all",
      "duration-700",
      "drop-shadow-md",
      "space-y-4",
      "flex",
      "flex-col",
    ],
    {
      variants: {
        intent: {
          primary: [],
        },
        expanded: {
          true: ["w-80"],
          false: ["w-4 sm:w-24"]
        }
      },
      defaultVariants: {
        intent: "primary",
      },
    }
)

export interface SidebarProps extends React.BaseHTMLAttributes<HTMLDivElement>, VariantProps<typeof sidebar> {
  underline?: boolean
  expanded?: boolean
  handleExpandedChange: (state: boolean) => void
}

export function Sidebar({className, intent, expanded = false, handleExpandedChange, ...props}: SidebarProps) {
  return (
      <div className={twMerge(sidebar({intent: intent, expanded: expanded, className}))} {...props}>
        <div className="flex flex-col max-h-full h-full">
          {props.children}
        </div>
        <Button size="round" intent="tertiary" href="#" onClick={() => {
          handleExpandedChange(!expanded)
        }} className="absolute -right-5 md:-right-4 top-16">
          <DoubleArrowLeftIcon
              className={"duration-700 transition-translation".concat(expanded ? "" : " -rotate-180")}/>
        </Button>
      </div>
  )
}