import type { Meta, StoryObj } from "@storybook/react"
import { Tooltip } from "./Tooltip"
import {Button} from "../Button/Button";

const meta: Meta<typeof Tooltip> = {
  title: "Tooltip",
  component: Tooltip,
  args: {
    explainer: "Test",
    intent: "primary",
    size: "md",
    withArrow: true
  },
  argTypes: {
    intent: {
      options: ["primary"],
      control: { type: "select" },
    },
    size: {
      options: ["md"],
      control: { type: "select" },
    },
    explainer: {type: "string"},
    withArrow: {type: "boolean"}
  },
}

type Story = StoryObj<typeof Tooltip>

export const Default: Story = {
  render: (args) => <p><Tooltip {...args}><span>Test</span></Tooltip></p>,
}

export default meta
