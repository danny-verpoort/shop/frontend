import type { Meta, StoryObj } from "@storybook/react"
import { Header } from "./Header"

const meta: Meta<typeof Header> = {
    title: "Header",
    component: Header,
    args: {
        intent: "primary",
        expanded: false
    },
    argTypes: {
        intent: {
            options: ["primary"],
            control: { type: "select" },
        },
        expanded: {type: "boolean"}
    },
}

type Story = StoryObj<typeof Header>

export const Default: Story = {
    render: (args) => <Header {...args} >Test</Header>,
    parameters: {
        layout: "fullscreen"
    }
}

export default meta