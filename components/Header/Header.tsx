import { cva, type VariantProps } from "class-variance-authority"
import { twMerge } from "tailwind-merge"
import Logo from "./Logo"

const header = cva(
    [
        "from-teal-600",
        "to-teal-700",
        "bg-gradient-to-br",
        "flex",
        "flex-row",
        "items-center",
        "justify-left",
        "transition-width",
        "duration-700",
        "h-24",
        "overflow-hidden",
        "flex-none",
        "pl-4",
    ],
    {
        variants: {
            intent: {
                primary: [],
            },
        },
        defaultVariants: {
            intent: "primary",
        },
    }
)

export interface SidebarProps extends React.BaseHTMLAttributes<HTMLAnchorElement>, VariantProps<typeof header> {
    underline?: boolean
    expanded?: boolean
}

export function Header({ className, intent, expanded = false, ...props }: SidebarProps) {
    return (
        <a href={props.href} className={twMerge(header({ intent: intent, className}))} {...props}>
          <Logo className="w-12 h-12 inline drop-shadow-xl"/>
          <span className={twMerge("ml-5 block text-lg font-medium text-white font-mono drop-shadow-sm font-oswald transition-all duration-700", expanded ? "w-32 opacity-1" : "w-0 opacity-0")}>{props.children}</span>
        </a>
    )
}