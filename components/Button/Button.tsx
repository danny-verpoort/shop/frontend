import { cva, type VariantProps } from "class-variance-authority"

import { twMerge } from "tailwind-merge"

const button = cva(
  [
    "justify-center",
    "inline-flex",
    "items-center",
    "rounded-xl",
    "text-center",
    "border",
    "border-blue-400",
    "transition-colors",
    "delay-50",
  ],
  {
    variants: {
      intent: {
        primary: ["bg-blue-400", "text-white", "hover:enabled:bg-blue-700"],
        secondary: ["bg-white", "text-blue-400", "hover:enabled:bg-blue-400", "hover:enabled:text-white"],
        tertiary: ["bg-white", "text-gray-700", "border-none", "outline", "outline-2", "outline-teal-500"]
      },
      size: {
        sm: ["min-w-20", "min-h-10", "text-sm", "py-1.5", "px-4"],
        lg: ["min-w-32", "min-h-12", "text-lg", "py-2.5", "px-6"],
        round: ["min-w-10", "min-h-10", "py-2", "px-2", "rounded-full"]
      },
      underline: { true: ["underline"], false: [""] },
    },
    defaultVariants: {
      intent: "primary",
      size: "lg",
    },
  }
)

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement>, VariantProps<typeof button> {
  underline?: boolean
  href: string
}

export function Button({ className, intent, size, underline, ...props }: ButtonProps) {
  return (
    <button className={twMerge(button({ intent, size, className, underline }))} {...props}>
      {props.children}
    </button>
  )
}
