import { cva, type VariantProps } from "class-variance-authority"

import { twMerge } from "tailwind-merge"
import {IconProps} from "@radix-ui/react-icons/dist/types";
import * as React from "react";
import {ReactElement} from "react";

const sidebarItem = cva(
    [
        "flex",
        "flex-row",
        "items-center",
        "justify-strech",
        "text-zinc-50",
        "font-normal",
        "text-md",
    ],
    {
        variants: {
            intent: {
                primary: [],
            },
        },
        defaultVariants: {
            intent: "primary",
        },
    }
)

export interface SidebarItemProps extends React.BaseHTMLAttributes<HTMLAnchorElement>, VariantProps<typeof sidebarItem> {
    expanded?: boolean,
    icon?: ReactElement
}

export function SidebarItem({ className, intent, expanded = false, icon, ...props }: SidebarItemProps) {
    return (
        <a className={twMerge(sidebarItem({ intent: intent, className}))} {...props}>
          <div className="ml-10 w-6 pt-1 h-full scale-[2]">
            {icon}
          </div>
          <div className={"pl-5 duration-700 transition-all ".concat(expanded ? "w-32 opacity-100" : "w-0 opacity-0")}>
            {props.children}
          </div>
        </a>
    )
}