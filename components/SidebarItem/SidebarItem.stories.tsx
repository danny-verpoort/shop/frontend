import type { Meta, StoryObj } from "@storybook/react"
import { SidebarItem } from "./SidebarItem"

const meta: Meta<typeof SidebarItem> = {
    title: "Sidebar Item",
    component: SidebarItem,
    args: {
        intent: "primary",
        expanded: false
    },
    argTypes: {
        intent: {
            options: ["primary"],
            control: { type: "select" },
        },
        expanded: {type: "boolean"}
    },
}

type Story = StoryObj<typeof SidebarItem>

export const Default: Story = {
    render: (args) => <SidebarItem {...args} >Test</SidebarItem>,
    parameters: {
        layout: "fullscreen"
    }
}

export default meta