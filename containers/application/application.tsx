import Head from "next/head"
import {Sidebar} from "../../components/Sidebar/Sidebar";
import {Header} from "../../components/Header/Header";
import useMenuExpandedState from "../../stores/menuExpanded"
import {SidebarItem} from "../../components/SidebarItem/SidebarItem";
import {AvatarIcon, CubeIcon, ExitIcon, ListBulletIcon, RocketIcon} from "@radix-ui/react-icons";
import * as ScrollArea from "@radix-ui/react-scroll-area";
import * as Avatar from '@radix-ui/react-avatar';
import {signOut} from "next-auth/react"

export default function Application(props: any) {
  const menuExpanded = useMenuExpandedState();
  return (
      <>
        <Head>
          <meta property="og:url" content="https://next-enterprise.vercel.app/"/>
          <meta
              property="og:image"
              content="https://raw.githubusercontent.com/Blazity/next-enterprise/main/project-logo.png"
          />
          <meta property="og:image:width" content="1200"/>
          <meta property="og:image:height" content="630"/>
          <meta name="twitter:card" content="summary_large_image"/>
          <title>Awesome Webshop</title>
        </Head>
        <div className="flex flex-row space-x-2 md:space-x-10 w-full" style={{width: "100%"}}>
          <Sidebar expanded={menuExpanded.expanded} handleExpandedChange={menuExpanded.setExpanded} className="mr-4">
            <Header expanded={menuExpanded.expanded} href="/">
              StockManager
            </Header>
            <ScrollArea.Root className="ScrollAreaRoot">
              <ScrollArea.Viewport className="ScrollAreaViewport">
                <div className="flex flex-col h-full space-y-8 pt-8">
                  <SidebarItem href="/categories" expanded={menuExpanded.expanded}
                               icon={<ListBulletIcon/>}>Categories</SidebarItem>
                  <SidebarItem href="/products" expanded={menuExpanded.expanded}
                               icon={<CubeIcon/>}>Products</SidebarItem>
                  <hr className="border-zinc-50 opacity-40 mx-4"/>
                  <SidebarItem href="/sales" expanded={menuExpanded.expanded} icon={<RocketIcon/>}>Sales</SidebarItem>
                  <hr className="border-zinc-50 opacity-40 mx-4"/>
                  <SidebarItem href="/users" expanded={menuExpanded.expanded} icon={<AvatarIcon/>}>Users</SidebarItem>
                  <div className="grow flex flex-col justify-end space-y-4">
                    <SidebarItem className="pb-8 mt-4" href="logout" expanded={menuExpanded.expanded}
                                 icon={<Avatar.Root className="AvatarRoot w-4 aspect-square">
                                   <Avatar.Image
                                       className="AvatarImage"
                                       src="https://scontent-ams4-1.xx.fbcdn.net/v/t31.18172-8/12080252_10153182396733806_7951826544856842318_o.jpg?_nc_cat=102&ccb=1-7&_nc_sid=174925&_nc_ohc=VwKUn57BO4UAX_NArfP&_nc_ht=scontent-ams4-1.xx&oh=00_AfAMLHlEBNtXxgLXNbrxs5EapoBbrQW3XjOn9cR_0lQE_Q&oe=64B81C40"
                                       alt="Danny Verpoort"
                                   />
                                   <Avatar.Fallback className="AvatarFallback text-[8px]" delayMs={600}>
                                     DV
                                   </Avatar.Fallback>
                                 </Avatar.Root>}><p className="mt-1">Profile</p></SidebarItem>
                    <hr className="border-zinc-50 opacity-40 mx-4"/>
                    <SidebarItem className=" pt-8 pb-8" onClick={() => signOut()} expanded={menuExpanded.expanded}
                                 icon={<ExitIcon/>}>Logout</SidebarItem>
                  </div>
                </div>
              </ScrollArea.Viewport>
              <ScrollArea.Scrollbar className="ScrollAreaScrollbar" orientation="vertical">
                <ScrollArea.Thumb className="ScrollAreaThumb"/>
              </ScrollArea.Scrollbar>
            </ScrollArea.Root>
          </Sidebar>
          <div className="h-[calc(100dvh)] grow">
            <ScrollArea.Root className="ScrollAreaRoot">
              <ScrollArea.Viewport className="ScrollAreaViewport">
                <div className="py-8 pr-2 md:pr-10">
                  {props.children}
                </div>
              </ScrollArea.Viewport>
              <ScrollArea.Scrollbar className="ScrollAreaScrollbar" orientation="vertical">
                <ScrollArea.Thumb className="ScrollAreaThumb"/>
              </ScrollArea.Scrollbar>
            </ScrollArea.Root>
          </div>
        </div>
      </>
  )
}