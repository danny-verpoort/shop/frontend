import { create } from 'zustand'
import { devtools, persist } from 'zustand/middleware'

interface menuExpandedState {
  expanded: boolean
  setExpanded: (expanded: boolean) => void
}

const useMenuExpandedState = create<menuExpandedState>()(
  devtools(
    persist(
      (set) => ({
        expanded: false,
        setExpanded: (expandedUpdate) => set((state) => ({ expanded: expandedUpdate })),
      }),
      {
        name: 'menu-expanded',
      }
    )
  )
)

export default useMenuExpandedState